import threading
from PyTango.server import run
from PyTango.server import Device
from PyTango.server import pipe
from PyTango import PipeWriteType
import psutil


class ProcessManager(Device):

    Processes = pipe(access=PipeWriteType.PIPE_READ)
    StartProcesses = pipe(access=PipeWriteType.PIPE_READ_WRITE)

    def read_Processes(self):
        data = ('root', [])
        last = data
        for p in psutil.process_iter():
            process_data = [
                {'name': 'pid', 'value': p.pid},
                {'name': 'ppid', 'value': p.ppid()},
                {'name': 'nice', 'value': p.nice()},
                {'name': 'cpu_affinity', 'value': p.cpu_affinity()},
                {'name': 'name', 'value': p.name()},
                {'name': 'cmdline', 'value': " ".join(p.cmdline())},
                {'name': 'username', 'value': p.username()},
            ]
            process_blob = (str(p.pid), process_data)
            last[1].append({'name': 'next', 'value': process_blob})
            last = process_blob
        return data[1][0]['value'] if len(data[1]) > 0 else ()

    def read_StartProcesses(self):
        return ('read-only', [{'name': 'dummy', 'value': True}])

    def write_StartProcesses(self, data):
        last = data
        while True:
            proc_data = {e['name']: e['value'] for e in last[1]}
            self.start_process(proc_data)
            if 'next' not in proc_data:
                break
            last = proc_data['next']

    def start_process(self, proc_data):
        print(proc_data)
        t = threading.Thread(target=self.run_in_thread, args=(proc_data,))
        t.daemon = True
        t.start()

    def run_in_thread(self, proc_data):
        proc = psutil.Popen(proc_data['commandline'])
        proc.nice(proc_data['nice'])
        proc.cpu_affinity(proc_data['cpu_affinity'])
        proc.wait()

if __name__ == "__main__":
    run((ProcessManager,))
