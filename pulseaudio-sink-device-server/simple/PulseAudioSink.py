import pulsectl
import tango
from tango.server import Device, attribute, command, device_property, run


class PulseAudioSink(Device):

    SinkName = device_property(
        dtype=str,
        default_value='@DEFAULT_SINK@')

    Volume = attribute(
        dtype=float,
        access=tango.AttrWriteType.READ_WRITE,
        min_value=0,
        max_value=1)

    def init_device(self):
        super(PulseAudioSink, self).init_device()
        self.pulse = pulsectl.Pulse(self.get_name())
        self.set_state(tango.DevState.ON)

    def delete_device(self):
        self.pulse.close()
        super(PulseAudioSink, self).delete_device()

    def get_sink(self):
        return self.pulse.get_sink_by_name(self.SinkName)

    def read_Volume(self):
        return self.pulse.volume_get_all_chans(self.get_sink())

    def write_Volume(self, value):
        self.pulse.volume_set_all_chans(self.get_sink(), value)

    @attribute(dtype=str)
    def Description(self):
        return self.get_sink().description

    @command
    def Toggle(self):
        sink = self.get_sink()
        mute = bool(sink.mute)
        self.pulse.sink_mute(sink.index, not mute)


if __name__ == "__main__":
    run((PulseAudioSink,))
