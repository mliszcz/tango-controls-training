# pylint: disable=missing-docstring,redefined-outer-name,unused-argument

from unittest import mock
from tango.test_context import DeviceTestContext
import pytest
from PulseAudioSink import PulseAudioSink


@pytest.fixture
def pulse():
    with mock.patch('pulsectl.Pulse') as pulse_ctor:
        yield pulse_ctor.return_value


@pytest.fixture
def proxy(pulse):
    with DeviceTestContext(PulseAudioSink) as proxy:
        yield proxy


def test_read_volume(proxy, pulse):
    value = 0.38
    pulse.volume_get_all_chans.return_value = value
    assert proxy.Volume == value
    pulse.volume_get_all_chans.assert_called_once()


def test_write_volume(proxy, pulse):
    value = 0.42
    proxy.Volume = value
    pulse.volume_set_all_chans.assert_called_once_with(mock.ANY, value)
