# pylint: disable=missing-docstring,redefined-outer-name,unused-argument

from unittest import mock
import tango
from tango.test_context import DeviceTestContext
import pytest
from PulseAudioSink import PulseAudioSink


@pytest.fixture
def pulse():
    with mock.patch('pulsectl.Pulse') as pulse_ctor:
        yield pulse_ctor.return_value


@pytest.fixture
def proxy(pulse):
    with DeviceTestContext(PulseAudioSink) as proxy:
        yield proxy


def test_read_volume(proxy, pulse):
    value = 0.38
    pulse.volume_get_all_chans.return_value = value
    assert proxy.Volume == value
    pulse.volume_get_all_chans.assert_called_once()


def test_write_volume(proxy, pulse):
    value = 0.42
    proxy.Volume = value
    pulse.volume_set_all_chans.assert_called_once_with(mock.ANY, value)


@pytest.mark.parametrize('value', [True, False])
def test_read_mute(proxy, pulse, value):
    prop = mock.PropertyMock(return_value=value)
    type(pulse.get_sink_by_name.return_value).mute = prop
    assert proxy.Mute == value
    assert prop.mock_calls == [mock.call(), mock.call()]


@pytest.mark.parametrize('value', [True, False])
def test_write_mute(proxy, pulse, value):
    proxy.Mute = value
    pulse.sink_mute.assert_called_once_with(
        pulse.get_sink_by_name.return_value.index,
        value)


def test_initialize_with_custom_sink_name(pulse):
    sink_name = "MyCustomSinkName"
    properties = {'SinkName': sink_name};
    with DeviceTestContext(PulseAudioSink, properties=properties) as proxy:
        _ = proxy.State()
        pulse.get_sink_by_name.assert_called_once_with(sink_name)


@pytest.mark.parametrize('initial, expected', [(True, False), (False, True)])
def test_toggle_command(proxy, pulse, initial, expected):
    prop = mock.PropertyMock(return_value=initial)
    type(pulse.get_sink_by_name.return_value).mute = prop
    pulse.attach_mock(prop, 'prop')
    proxy.Toggle()
    pulse.assert_has_calls(
        [mock.call.prop(), mock.call.sink_mute(mock.ANY, expected)])


def test_start_in_on_state(proxy, pulse):
    pulse.get_sink_by_name.return_value.mute = False
    assert proxy.State() == tango.DevState.ON


def test_go_to_disabled_state_when_muted(proxy, pulse):
    pulse.get_sink_by_name.return_value.mute = True
    assert proxy.State() == tango.DevState.DISABLE


def test_go_to_fault_state_when_sink_is_not_available(proxy, pulse):
    error = "some failure!"
    pulse.get_sink_by_name.side_effect = Exception(error)
    assert proxy.State() == tango.DevState.FAULT
    assert proxy.Status() == (
        "The device is in FAULT state.\n"
        "Failed to access sink: Exception('some failure!',)")

