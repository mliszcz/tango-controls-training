# noqa: E501 pylint: disable=invalid-name,missing-module-docstring,missing-class-docstring
# noqa: E501 pylint: disable=missing-function-docstring,no-else-return,attribute-defined-outside-init

import asyncio
import functools
import logging

import tango

import dbus_next
from dbus_next.aio import MessageBus
from dbus_next.proxy_object import BaseProxyInterface

from tango.server import Device, command, device_property, get_worker

from dbus_next_direct import DirectMessageBus


logger = logging.getLogger(__name__)

server_started = asyncio.get_event_loop().create_future()

to_snake_case = BaseProxyInterface._to_snake_case  # noqa E501 pylint: disable=protected-access


def run_in_executor(fn):
    @functools.wraps(fn)
    def wrapper(*args, **kwargs):
        return get_worker().execute(fn, *args, **kwargs)
    return wrapper


def build_dbus_bus(address, bus_name):
    if address:
        if bus_name:
            return MessageBus(address)
        else:
            return DirectMessageBus(address)
    else:
        return MessageBus()


def dbus_to_tango_type(code):
    array = True
    if code in list('b'):
        return not array, tango.DevBoolean
    if code in list('ynqiuxth'):
        return not array, tango.DevLong
    if code in list('d'):
        return not array, tango.DevDouble
    if code in list('sog'):
        return not array, tango.DevString
    if len(code) == 2 and code[0] == "a":
        return array, dbus_to_tango_type(code[1])[1]
    raise ValueError("Unsupported D-Bus data type: {}".format(code))


def dbus_to_tango_access_mode(access):
    if access == dbus_next.PropertyAccess.READ:
        return tango.READ
    if access == dbus_next.PropertyAccess.WRITE:
        return tango.WRITE
    if access == dbus_next.PropertyAccess.READWRITE:
        return tango.READ_WRITE
    raise ValueError("Unsupported access mode: {}".format(access))


def get_inerface_by_name(introspection, name):
    iface = (itf for itf in introspection.interfaces if itf.name == name)
    return next(iface, None)


class DBusObject(Device):

    green_mode = tango.GreenMode.Asyncio

    Address = device_property(dtype=str)
    BusName = device_property(dtype=str)
    ObjectPath = device_property(dtype=str, mandatory=True)
    Interface = device_property(dtype=str, mandatory=True)

    async def init_device(self):
        await super().init_device()

        self._dynamic_attributes = []
        self._dynamic_commands = []

        bus = build_dbus_bus(self.Address, self.BusName)
        self.bus = await bus.connect()

        introspection = await bus.introspect(
            self.BusName, self.ObjectPath, timeout=1)

        self.proxy = self.bus.get_proxy_object(
            self.BusName, self.ObjectPath, introspection)

        self.set_state(tango.DevState.INIT)

        self.interface = None

        def create_dynamic_interface(_):
            interface = get_inerface_by_name(introspection, self.Interface)
            if interface:
                self.interface = self.proxy.get_interface(interface.name)
                self.create_dynamic_attributes(interface.properties)
                self.create_dynamic_commands(interface.methods)
                self.set_state(tango.DevState.ON)
            else:
                self.set_state(tango.DevState.STANDBY)

        server_started.add_done_callback(create_dynamic_interface)

    async def delete_device(self):
        self.remove_dynamic_attributes()
        self.remove_dynamic_commands()
        self.bus.disconnect()

    def remove_dynamic_attributes(self):
        for attr in self._dynamic_attributes:
            try:
                self.remove_attribute(attr)
            except Exception:  # pylint: disable=broad-except
                logger.exception()
        self._dynamic_attributes = []

    def remove_dynamic_commands(self):
        for cmd in self._dynamic_commands:
            try:
                self.remove_command(cmd)
            except Exception:  # pylint: disable=broad-except
                logger.exception()
        self._dynamic_commands = []

    def create_dynamic_attributes(self, properties):
        for prop in properties:
            try:
                logger.info("Adding attribute: %s", prop.name)
                self.add_one_dynamic_attribute(prop)
            except Exception as e:  # pylint: disable=broad-except
                logger.error("Failed to add attribute: %s: %r", prop.name, e)

    def add_one_dynamic_attribute(self, dbus_prop):
        name = dbus_prop.name
        access = dbus_to_tango_access_mode(dbus_prop.access)
        is_array, dtype = dbus_to_tango_type(dbus_prop.type.signature)

        if is_array:
            attribute = tango.SpectrumAttr(name, dtype, access, 1024)
        else:
            attribute = tango.Attr(name, dtype, access)

        self.add_attribute(
            attribute,
            self.get_dynamic_attribute,
            self.set_dynamic_attribute)

        self._dynamic_attributes.append(name)

    def create_dynamic_commands(self, methods):
        for meth in methods:
            try:
                logger.info("Adding command: %s", meth.name)
                self.add_one_dynamic_command(meth)
            except Exception as e:  # pylint: disable=broad-except
                logger.error("Failed to add command: %s: %r", meth.name, e)

    def add_one_dynamic_command(self, dbus_method):
        # pylint: disable=too-many-locals
        name = dbus_method.name
        in_args = [arg.signature for arg in dbus_method.in_args]
        out_args = [arg.signature for arg in dbus_method.out_args]
        if len(in_args) > 1:
            raise ValueError("Too many input arguments: {}".format(in_args))
        if len(out_args) > 1:
            raise ValueError("Too many output arguments: {}".format(in_args))

        def first_arg_to_tango_type_or_void(args):
            if args:
                return dbus_to_tango_type(args[0])
            else:
                return False, tango.DevVoid

        in_is_array, in_dtype = first_arg_to_tango_type_or_void(in_args)
        out_is_array, out_dtype = first_arg_to_tango_type_or_void(out_args)

        dformat_in = tango.AttrDataFormat.SPECTRUM if in_is_array else None
        dformat_out = tango.AttrDataFormat.SPECTRUM if out_is_array else None

        @run_in_executor
        async def call_dynamic_command(*args):  # noqa: E501 pylint: disable=unused-argument
            nonlocal self
            method_name = to_snake_case(dbus_method.name)
            return await eval("self.interface.call_{}(*args)".format(method_name))  # noqa: E501 pylint: disable=eval-used

        call_dynamic_command.__name__ = name
        setattr(self, name, call_dynamic_command)

        cmd = command(
            f=call_dynamic_command,
            dtype_in=in_dtype,
            dtype_out=out_dtype,
            dformat_in=dformat_in,
            dformat_out=dformat_out)
        only_for_this_device = True
        self.add_command(cmd, only_for_this_device)
        self._dynamic_commands.append(name)

    @run_in_executor
    async def get_dynamic_attribute(self, attribute):
        name = to_snake_case(attribute.get_name())
        getter = getattr(self.interface, "get_{}".format(name))
        value = await getter()
        attribute.set_value(value)

    @run_in_executor
    async def set_dynamic_attribute(self, attribute):
        name = to_snake_case(attribute.get_name())
        setter = getattr(self.interface, "set_{}".format(name))
        value = attribute.get_write_value()
        await setter(value)

    @command(
        dtype_in=(str,),
        dtype_out=str,
        display_level=tango.DispLevel.EXPERT)
    async def CallMethod(self, argin):
        iface_name, method_name, args = argin
        interface = self.proxy.get_interface(iface_name)  # noqa: E501,F841 pylint: disable=unused-variable
        name = to_snake_case(method_name)
        result = await eval("interface.call_{}({})".format(name, args))  # noqa: E501 pylint: disable=eval-used
        if isinstance(result, dbus_next.Variant):
            return str(result.value)
        else:
            return str(result)


if __name__ == '__main__':
    logging.basicConfig(
        format="%(asctime)s %(levelname)s [%(threadName)s] %(filename)s:%(lineno)d: %(message)s",  # noqa: E501 pylint: disable=line-too-long
        level=logging.DEBUG)

    def post_init():
        server_started.set_result(None)

    DBusObject.run_server(post_init_callback=post_init)
