import asyncio

from dbus_next.constants import BusType
from dbus_next.message_bus import BaseMessageBus
from dbus_next.proxy_object import BaseProxyObject
from dbus_next._private.unmarshaller import Unmarshaller
from dbus_next.aio import MessageBus, ProxyObject
from dbus_next._private.auth import auth_external, auth_parse_line, auth_begin, AuthResponse


__all__ = ('DirectMessageBus',)


class DirectProxyObject(ProxyObject):

    def __init__(self, bus_name, path, introspection, bus):
        # pass dummy name to satisfy validator
        dummy_bus_name = "d.u.m.m.y"
        super().__init__(dummy_bus_name, path, introspection, bus)
        self.bus_name = dummy_bus_name

    def get_interface(self, name):
        bus_call = self.bus._call
        # no need to perform any calls here
        self.bus._call = lambda msg, callback: None
        try:
            return super().get_interface(name)
        finally:
            self.bus._call = bus_call


class DirectMessageBus(MessageBus):

    def __init__(self, bus_address):
        BaseMessageBus.__init__(self, bus_address, BusType.SESSION, DirectProxyObject)
        self._loop = asyncio.get_event_loop()
        self._unmarshaller = Unmarshaller(self._stream)

    async def connect(self):
        await self._loop.sock_sendall(self._sock, b'\0')
        await self._loop.sock_sendall(self._sock, auth_external())
        response, args = auth_parse_line(await self._auth_readline())

        if response != AuthResponse.OK:
            raise AuthError(f'authorization failed: {response.value}: {args}')

        self._stream.write(auth_begin())
        self._stream.flush()

        self._loop.add_reader(self._fd, self._message_reader)

        self.unique_name = "peer"

        return self
