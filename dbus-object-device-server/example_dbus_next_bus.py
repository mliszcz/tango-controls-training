import asyncio
from dbus_next.aio import MessageBus

async def main():
    bus_name = 'org.pulseaudio.Server'
    object_path = '/org/pulseaudio/server_lookup1'
    bus = await MessageBus().connect()
    introspection = await bus.introspect(bus_name, object_path)
    proxy = bus.get_proxy_object(bus_name, object_path, introspection)
    interface = proxy.get_interface('org.PulseAudio.ServerLookup1')
    address = await interface.get_address()
    print(address)
    bus.disconnect()

if __name__ == "__main__":
    loop = asyncio.get_event_loop()
    loop.run_until_complete(main())
