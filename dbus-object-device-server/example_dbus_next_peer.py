import asyncio
from dbus_next_direct import DirectMessageBus

async def main():
    address = 'unix:path=/run/user/1000/pulse/dbus-socket'
    object_path = '/org/pulseaudio/core1/sink0'
    bus = await DirectMessageBus(address).connect()
    introspection = await bus.introspect(None, object_path)
    proxy = bus.get_proxy_object(None, object_path, introspection)
    interface = proxy.get_interface('org.PulseAudio.Core1.Device')
    volume = await interface.get_volume()
    print(volume)
    bus.disconnect()

if __name__ == "__main__":
    loop = asyncio.get_event_loop()
    loop.run_until_complete(main())
