import sys
from os.path import expanduser
from taurus.external.qt import Qt
from taurus.qt.qtgui.application import TaurusApplication
from taurus.qt.qtgui.util.ui import UILoadable

@UILoadable(with_ui='_ui')
class EyeOfGnomeUI(Qt.QWidget):
    def __init__(self):
        super(EyeOfGnomeUI, self).__init__()
        self.loadUi('EyeOfGnomeUI.ui', path=expanduser('.'))

app = TaurusApplication(sys.argv)

eog = EyeOfGnomeUI()
eog.show()

sys.exit(app.exec_())
